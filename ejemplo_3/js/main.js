window.addEventListener("load", function(){
/*
    var lista = [
        [2, "Juan", "34"],
        [5, "Juan", "34"],
        [7, "Pedro", "34"],
        [12, "Juan", "34"],
        [32, "Jorge", "34"],
        [62, "Juan", "34"],
        [82, "Juan", "34"],
        [232, "Ana", "34"],
    ]

    for(var i=0; i<lista.length; i++){
        if (lista[i][0] == 32) {
            console.log(lista[i][1]);
        }
    }
*/

    var listaAsociativa = {
        usuario2: {nombre: "Juan", edad: "34"},
        usuario5: {nombre: "Juan", edad: "34"},
        usuario7: {nombre: "Pedro", edad: "34"},
        usuario12: {nombre: "Juan", edad: "34"},
        usuario32: {nombre: "Jorge", edad: "34"},
        usuario62: {nombre: "Juan", edad: "34"},
        usuario82: {nombre: "Ana", edad: "34"},
    }

    /*
    // recorremos el array asociativo mediante el bucle for in
    for(var prop in listaAsociativa){
        // recuperamos el usuario mediante su nombre dado por la variable prop del bucle
        var usuario = listaAsociativa[prop];
        // recuperamos la referencia del contenedor ul mediante su id
        var contenedor = document.getElementById('listaUsuarios');
        // creamos una nueva etiqueta li
        var nuevoLi = document.createElement('li');
        // creamos el nodo de texto con el nombre, que recuperamos de
        // la propiedad nombre del usuario
        var texto = document.createTextNode(usuario.nombre);
        // añadimos el texto a la etiqueta li
        nuevoLi.appendChild(texto);
        // añadimos la etiqueta li al contenedor
        contenedor.appendChild(nuevoLi);
    }
*/
    function reconstruyeLista(){
        var contenedor = document.getElementById('listaUsuarios');
        contenedor.innerHTML = "";
        for(var prop in listaAsociativa){
            // recuperamos el usuario mediante su nombre dado por la variable prop del bucle
            var usuario = listaAsociativa[prop];
            creaUsuario(usuario, prop);
        }
    }
    reconstruyeLista();


    function creaUsuario(usuario, prop){
        // recuperamos la referencia del contenedor ul mediante su id
        var contenedor = document.getElementById('listaUsuarios');

        var li = addTag('li', '', contenedor, 'fila');
        addTag('div', prop, li, 'columna');
        addTag('div', usuario.nombre, li, 'columna');
        addTag('div', usuario.edad, li, 'columna');
    }

    function addTag(tag, text, parent, clase){
        var nuevoTag = document.createElement(tag);
        nuevoTag.classList.add(clase);
        var texto = document.createTextNode(text);
        nuevoTag.appendChild(texto);
        parent.appendChild(nuevoTag);

        return nuevoTag;
    }


    document.getElementById('boton_add').addEventListener('click', function(){
        var nombre = document.getElementById('nombre').value;
        var edad = document.getElementById('edad').value;


        var usuario = {
            nombre: nombre,
            edad: edad,
        }

        var ahora = Date.now();
        listaAsociativa['usuario_'+ ahora] = usuario;

        reconstruyeLista();
    })
})
