/*
Objeto fecha con el momento actual
 */
var hoy = new Date();
console.log(hoy.getHours() + ":" + hoy.getMinutes() + ":" + hoy.getSeconds());
/*
fecha de hace un año
 */
var otraFecha = new Date(2018,4,8);
console.log(otraFecha.getFullYear());
