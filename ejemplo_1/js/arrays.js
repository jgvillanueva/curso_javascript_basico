/*
Uso de arrays
 */
var lista = [1,2,3,4];
console.log(lista[3]);
lista[0] = 5;
console.log(lista);

/*
Array de objetos
 */
var usuarios = [];
usuarios[0] = creaPersona(22, "Juan");
usuarios[1] = creaPersona(16, "Ana");
usuarios[2] = creaPersona(65, "Pedro");
usuarios[3] = creaPersona(23, "Jorge");
console.log(usuarios[2].nombre);

/*
Creación de arrays con operador new
 */
var lista2 = new Array(1,2,3,4);



