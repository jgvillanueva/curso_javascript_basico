/*
Ámbito de las variables
 */
var dato = 0;
function funcionPrimaria() {
    var dato = 1;

    console.log('Dato local', dato);
}
console.log(dato);
funcionPrimaria();
console.log('Dato global', dato);

/*
Uso de funciones y parámetros
 */
function saluda(nombre, texto) {
    console.log("Recuperando parámetros de la función: " + texto + ': ' + nombre);
}
saluda('jorge', 'Hola');


/*
Funciones para crear objetos
 */
function creaPersona(edad, nombre) {
    var persona = {
        edad: edad,
        nombre: nombre,
        nacimiento: function(){
            return 2019 - this.edad;
        }
    }
    return persona;
}
var persona1 = creaPersona(33, "Paco");
console.log(persona1);
console.log(persona1.nacimiento());

/*
Objetos usando operador new
 */
var obj = new Object();
obj.edad = 23;
obj.nombre = 'Juan';
