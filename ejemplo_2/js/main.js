
window.addEventListener("load", function(){
    console.log("App cargada, evento load");

    // recupera el contenedor principal mediante su id
    var maincontainer = document.getElementById('main-container');
    // recupera el bloque de navegación mediante su id
    var nav = document.getElementById('navegacion');
    // recupera TODOS los enlaces li del bloque de navegación
    // por eso utiliza nav en vez de document
    var enlaces = nav.getElementsByTagName('li');

    // bucle que recorre todos los elementos contenidos en el array enlaces
    for(var i=0; i < enlaces.length; i++){
        //a cada enace se le añade un evento click
        enlaces[i].addEventListener('click', function(){
            // crea una nueva etiqueta li
            var nuevoLi = document.createElement('li');
            // recupera el texto del enlace y lo guarda en un nodo de texto
            var texto = document.createTextNode(this.innerHTML);
            // añade el nuevo nodo de texto a la etiqueta li
            nuevoLi.appendChild(texto);

            // recupera el listado (ul) que está dentro del contenedor principal
            //var ul = maincontainer.getElementsByTagName('ul')[0];
            var ul = document.querySelector('#main-container ul');
            // añade el nuevo nodo dentro de ese listado
            ul.appendChild(nuevoLi);



            this.parentNode.removeChild(this);
        })
    }

    // evento para recuperar el click sobre el botón con el id boton_ocultar
    document.getElementById('boton_ocultar').addEventListener('click', function(){
        //maincontainer.classList.add('hidden');
        //maincontainer.classList.remove('hidden');
        maincontainer.classList.toggle('hidden');
    })

    // listado de usuarios (array con objetos dentro)
    // cada objeto lleva los datos del usuario
    var usuarios = [
        {
            edad: 22,
            nombre: 'Marcos',
            direccion: 'ssdkjfsklfhj sdfef'
        },
        {
            edad: 42,
            nombre: 'Pedro',
            direccion: '324sdf sdf sdfef'
        }
    ];

    // recupera el contenedor donde meteremos las etiquetas con los usuarios
    var contenedorUsuarios = document.getElementById('contenedorUsuarios');
    //bucle que recorre el listado de usuarios
    for(var i=0; i < usuarios.length; i++){
        // recupero el usuario propio de la iteración
        var usuario = usuarios[i];
        // creo la nueva etiqueta div
        var nuevoDiv = document.createElement('div');
        // recupera el nombre del usuario y lo guarda en un nodo de texto
        var texto = document.createTextNode(usuario.nombre);
        // añade el nuevo nodo de texto a la etiqueta div
        nuevoDiv.appendChild(texto);
        // y para terminar, añadimos la etiqueta div al contenedor de usuarios
        contenedorUsuarios.appendChild(nuevoDiv);
    }
})

/*
Otra forma de lanzar el evento load sobre el documento completo
 */
/*
window.onload = function(){
    console.log("App cargada, método onload");
}
*/

/*
function init(){
    console.log("Body cargado, evento onLoad");
}
*/
