window.addEventListener('load', function(){
    //recupero la referencia del contenedor del reloj
    var contenedorReloj = document.getElementById('reloj');

    // a esta función se la llama en cada ciclo del intervalo
    function actualizaReloj(){
        //guardo la referencia actual del tiempo con una nueva instancia de la clase Date
        var now = new Date();
        //creo el contenido nuevo de tiempo
        contenedorReloj.innerHTML =
            formatNumber(now.getHours()) + ':'
            + formatNumber(now.getMinutes()) + ':'
            + formatNumber(now.getSeconds());
    }
    // función que formatea para que los valores menores de 10 salgan con un cero delante
    function formatNumber(value){
        if(value < 10){
            return '0'+value;
        }
        //en caso de no ser menor de 10, devuelve el valor tal cual
        return value;
    }
    //se lanza el intervalo
    setInterval(actualizaReloj, 100);

    //defino una variable donde guardar la referencia al crono
    var intervalo;
    //añado el click para poner en marcha el crono
    document.getElementById('b_crono').addEventListener('click', function(){
        //guardo la referencia al tiempo actual
        var start = new Date();

        function actualizaCrono(){
            var now = new Date();
            var tiempo = now - start;
            document.getElementById('crono').innerHTML =
                Math.floor(tiempo/1000);
        }
        if(intervalo) {
            this.innerHTML = "start";
            clearInterval(intervalo);
            intervalo = null;
        }else{
            this.innerHTML = "stop";
            intervalo = setInterval(actualizaCrono, 100);
        }

    })
})
