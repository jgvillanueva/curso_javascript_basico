$(document).ready(function(){
    //variable sobre la que se almacenan os datos
    var data;

    // objeto de configuración de la llamada ajax
    var conf = {
        // método: get, post, put, ...
        method: 'get',
        //url donde está el servicio
        url: 'http://dev.contanimacion.com/birds/public/getBirds/1',
        //data: {},
        // formato en que se envían los datos
        dataType: 'json'
    }
    //petición ajax
    $.ajax(conf)
        //función que se ejecuta al recibir los datos
        .done(function(response){
            //guardamos los datos en la variable
            data = response;
            pintaTabla();
        })
        //función que se ejecuta si hay un error en la petición
        .fail(function(error){
            console.log("Ha ocurrido un error en la llamada");
        })

// función que pinta los datos recibidos
    function pintaTabla(){
        //referencia a la etiqueta ul donde se creará la tabla
        var ul = $('#listado');
        //bucle que recorre el array de datos
        for(var i=0; i<data.length; i++){
            var bird = data[i];
            // fila li
            var li = $('<li></li>')
                .append('<h3>'+bird.bird_name+'</h3>');
            // añade la fila li
            ul.append(li);
        }
    }
});
