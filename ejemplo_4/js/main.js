window.addEventListener("load", function(){
    // var seleccionado;
    var enlaces = document.querySelectorAll('header nav li');

    // en este ejemplo recupera la url para marcar el botón relacionado con la url
    var urlInicial = window.location.href.toLowerCase();
    // split parte en un array un string. Gracias a ello puedo recuperar
    //la última parte de la url
    var urlPartida = urlInicial.split('/');
    // usando el mismo sistema le quito el .html
    var pagina = urlPartida[urlPartida.length - 1];
    var nombrePagina = pagina.split('.')[0];

    //recorro con un bucle todos los enlaces
    for(var i=0; i<enlaces.length; i++){
        // meto en la variable elemento cada enlace
        var elemento = enlaces[i];

        //en caso de que el texto del enlace cohincia con el nombre de
        // la página html, hago que esté activo añadiendo una clase
        if(elemento.getElementsByTagName('a')[0].innerHTML.toLowerCase() == nombrePagina){
            elemento.classList.add('activo');
        }

        // añado el evento click a cada enlace
        elemento.addEventListener('click', function(event){
            // evito que funcione el enlace interceptando el evento
            event.preventDefault();
            // elimino cualquier enlace activo
            document.querySelector('header nav li.activo').classList.remove('activo');
           // añado la clase activo sobre el enlace en el que se ha hecho click
            // para que aparezca resaltado
            this.classList.add('activo');

            // meto en el rótulo el texto del enlace
            document.getElementsByTagName('h1')[0].innerHTML =
                this.getElementsByTagName('a')[0].innerHTML;
            /*
            if(seleccionado){
                seleccionado.classList.remove('activo');
            }
                seleccionado = this;
             */

            // y tras 3 segundos, hago que cambie la página suando window.location
            var url = this.getElementsByTagName('a')[0].href;
            setTimeout(function(){
                window.location = url;
            }, 3000)
        })
    }
    // en caso de que no haya enlace activo, fuerzo que lo sea el primero, que es la home
    if(document.querySelector('header nav li.activo') == undefined){
        document.querySelector('header nav li').classList.add('activo');
    }

    // programo el botón volver para que usando la clase history, vuelva un lugar atrás
    var boton_volver = document.getElementById('b_volver');
    if(boton_volver != undefined){
        boton_volver.addEventListener('click', function(){
            history.go(-1);
        })
    }

})
