$(document).ready(function(){
    console.log('Página cargada');

    // array asociativo con los datos que generarán la lista
    var usuarios = {
        usuario2: {nombre: "Juan", edad: "34", imagen:'1.jpg'},
        usuario5: {nombre: "Ana", edad: "12", imagen:'2.jpg'},
        usuario7: {nombre: "Pedro", edad: "25", imagen:'3.jpg'},
        usuario12: {nombre: "Juan", edad: "46", imagen:'4.jpg'},
        usuario32: {nombre: "Pablo", edad: "12", imagen:'5.jpg'},
        usuario62: {nombre: "Julia", edad: "13", imagen:'6.jpg'},
        usuario82: {nombre: "Sonia", edad: "57", imagen:'7.jpg'},
        usuario322: {nombre: "Paco", edad: "34", imagen:'1.jpg'},
        usuario15: {nombre: "Pedro", edad: "12", imagen:'2.jpg'},
        usuario27: {nombre: "Jose", edad: "25", imagen:'3.jpg'},
        usuario122: {nombre: "Ana", edad: "46", imagen:'4.jpg'},
        usuario302: {nombre: "Luís", edad: "12", imagen:'5.jpg'},
        usuario622: {nombre: "Rubén", edad: "13", imagen:'6.jpg'},
        usuario812: {nombre: "Jorge", edad: "57", imagen:'7.jpg'},
    }

    // referencia al contenedor donde se crearán los li del listado
    var $ul = $('#listado ul');
    pintaTabla(usuarios);

    function pintaTabla(_usuarios){
        // elimina los eventos para liberar memoria
        $ul.find('.b-view').each(function(){
            $(this).off('click', viewImg);
        })
        $ul.find('.b-delete').each(function(){
            $(this).off('click', deleteLi);
        })
        //borra los contenidos
        $ul.html('');


        //bucle que recorre el array asociativo. Prop almacena el mnombre de
        //cada propiedad
        for(var prop in _usuarios){
            // almacenamos los datos del usuario
            var usuario = _usuarios[prop];
            //creamos la etiqueta li y la guardamos en una variable
            var li = $('<li ><h3>'+usuario.nombre+'</h3></li>');
            li.data('id', prop);

            // creo un contenedor donde almacenar la información detallada
            var contenedorAmpliado = $('<div class="contenedorAmpliado"></div>');
            // añado la etiqueta para poner la edad y la imagen
            var edad = $('<span>Edad: '+usuario.edad+'</span>');
            var imagen = $('<img />');
            //en la imagen, cambio el atributo src para definir la ruta de donde sacarla
            imagen.attr('src', 'assets/'+usuario.imagen);
            //añado los contenidos al contenedor
            contenedorAmpliado.append(edad);
            contenedorAmpliado.append(imagen);


            //creamos un contenewdor para los botones
            var botonera = $('<div class="botonera"></div>');

            //creo los botones de acción
            var bView = $('<button class="b-view">View</button>');
            var bDelete = $('<button class="b-delete">Delete</button>');

            // les aplico el evento click
            bView.on('click', viewImg);
            bDelete.on('click', deleteLi);

            //añado los botones
            botonera.append(bView);
            botonera.append(bDelete);
            // añado la botonera
            li.append(botonera);
            // añado el contenedor ampliado al final, para que aparezca debajo de los botones
            li.append(contenedorAmpliado);

            //y por fin lo añado todo al ul
            $ul.append(li);
        }

        // código que hace que se vea la vista ampliada
        function viewImg(){
            var li = $(this).parent().parent();
            li.find('.contenedorAmpliado')
                // cambia la clasew show para mostrar / ocultar
                .toggleClass('show')
        }
        // código que se ejcuta cuando se pulsa delete
        function deleteLi(){
            var li = $(this).parent().parent();
            // elimina del array asociativo el dato
            delete(usuarios[li.data('id')]);
            // reconstruye la tabla
            pintaTabla(usuarios);
        }
    }

    //filtro
    $('#texto').keyup(function(){
        //recupera el valor de filtrado
        var texto = $(this).val();
        //crea el nuevo array donde meter esos datos
        var filtrados = {};
        // recorre el array
        for(var prop in usuarios){
            var usuario = usuarios[prop];
            //compara si tiene el texto
            if(usuario.nombre.toLowerCase().indexOf(texto.toLowerCase()) != -1){
                //en caso de tener el texto, añade el dato al array filtrado
                filtrados[prop] = usuario;
            }
        }
        //pinta la tabla con los nuevos datos
        pintaTabla(filtrados);
    });

});
